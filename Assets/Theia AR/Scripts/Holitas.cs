﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Itinero;
using Itinero.IO.Osm;
using Itinero.LocalGeo;
using Itinero.Osm.Vehicles;
using OsmSharp;
using Itinero.Navigation.Instructions;

public class Holitas : MonoBehaviour {
    [SerializeField]
    private Text Cartel;

    private Router router;
    private int Radious;

    // Use this for initialization
    void Start ()
    {
        Radious = 50;

        StartCoroutine(DownloadRoutes());
    }

    private IEnumerator DownloadRoutes()
    {
        using (WWW www = new WWW("http://files.itinero.tech/data/OSM/planet/europe/luxembourg-latest.osm.pbf"))
        {
            while(!www.isDone)
            {
                yield return null;
                Cartel.text = "Downdloading map: " + (www.bytesDownloaded/1024/1024) + " of 22MB downloaded";
            }

            Cartel.text = "Download finished ";
            
            if (www.error == null)
            {
                Debug.Log("DEBUG length bytes: " + www.bytes.Length.ToString());

                using (Stream stream = new MemoryStream(www.bytes))
                {
                    Debug.Log("DEBUG length stream: " + stream.Length.ToString());
                    var routerDb = new RouterDb();

                    if(routerDb == null)
                    {
                        Debug.Log("DEBUG routerdb null");
                    }

                    router = new Router(routerDb);
                    if(router == null)
                    {
                        Debug.Log("DEBUG router is null");
                    }

                    try
                    {
                        Debug.Log("Loading osm data");
                        routerDb.LoadOsmData(stream, Vehicle.Car);
                        Debug.Log("Osm data loaded");
                    }
                    catch (Exception e)
                    {
                        Debug.Log("DEBUG error loadosm: " + e.StackTrace);
                        Cartel.text = "DEBUG error loadosm: " + e.Message;
                    }

                    try
                    {
                        var route = router.Calculate(Vehicle.Car.Fastest(), new Coordinate(49.75635954613685f, 6.095362901687622f),
                            new Coordinate(49.75263039062888f, 6.098860502243042f));
                        Cartel.text = "Route calculated, total distance: " + route.TotalDistance.ToString();
                    }
                    catch (Exception e)
                    {
                        Debug.Log("DEBUG error calculate: " + e.Message);
                        Cartel.text += "DEBUG error calculate: " + e.Message;
                    }
                } 
            }
            else
            {
                Cartel.text = "Download error";
            }
        }
    }
}
